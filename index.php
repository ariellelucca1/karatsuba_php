<?php

ini_set('MAX_EXECUTION_TIME', '-1');

    function karatsuba($x, $y, $tipo){
        $tamanho_x = strlen($x);
        $tamanho_y = strlen($y);

        if ($tamanho_x > $tamanho_y) $tamanho = $tamanho_x;
        else $tamanho = $tamanho_y;
        $x = intval($x);
        $y = intval($y);



        if ($tamanho <= 2) {
            echo $tipo . ' | x ' . $x . ' y ' . $y . ' = ' . $x*$y . '</p>';
            return $x*$y;
        }

        else{
            echo " x " . $x . " y " . $y;  
            $m = ceil($tamanho/2);
            $x_H = intval($x/(pow(10, $m)));
            $x_L = $x % (pow(10, $m));
            $y_H = intval($y/(pow(10, $m)));
            $y_L = $y % (pow(10, $m));  

            echo $tipo . ' | m' . $m . ' xh ' . $x_H . ' xl ' . $x_L . ' yh ' . $y_H . ' yl ' . $y_L . '</p>'; 

            $a = karatsuba($x_H, $y_H, 'a');
            $b = karatsuba($x_L, $y_L, 'b');
            $c = karatsuba($x_H + $x_L, $y_H + $y_L, 'c');
            $d = $a * (pow(10, 2*$m)) + ($c - $a - $b) * (pow(10, $m)) + $b;
            echo 'd = ' . $d . ' ' . $tipo . ' ' . $m . '</p>';
            return $d;
        }
    }

    echo karatsuba(12345678, 87654321, 'final');
    // resultado: 1082152022374638
?>